
    <footer id="footer">
      Scandiweb Test assignment
    </footer>
  <script>
    $(document).ready(function () {
      toggleFields(); // call this first so we start out with the correct visibility depending on the selected form values
      // this will call our toggleFields function every time the selection value of our other field changes
      $("#productType").change(function () {
          toggleFields($(this).val());
      });

    });
    // this toggles the visibility of other server
    function toggleFields(value) {
      switch(value){
        case "Dvd":
          $("#dvdInputs").show();
          $("#size").prop('required','true');

          $("#furnitureInputs").hide();
          $("#height").removeAttr('required');   
          $("#width").removeAttr('required');  
          $("#length").removeAttr('required');  

          $("#bookInputs").hide();
          $("#weight").removeAttr('required');  
          break;
        case "Furniture":
          $("#dvdInputs").hide();
          $("#size").removeAttr('required');   

          $("#furnitureInputs").show();
          $("#height").prop('required','true');
          $("#width").prop('required','true');
          $("#length").prop('required','true');

          $("#bookInputs").hide();
          $("#weight").removeAttr('required');
          break;
        case "Book":
          $("#dvdInputs").hide();
          $("#size").removeAttr('required');

          $("#furnitureInputs").hide();
          $("#height").removeAttr('required');
          $("#width").removeAttr('required');
          $("#length").removeAttr('required');

          $("#bookInputs").show();
          $("#weight").prop('required','true');
          break;
        default:
          break;
      }

    }
  </script>
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" ></script>
  <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
<html>
