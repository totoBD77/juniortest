<?php 
  
  use Source\Handlers\DeleteProductFormHandler;
  use Source\Views\{ProductsList, DvdView, BookView, FurnitureView};
  if (is_post_request()){

    
    $skuArray = $_POST['checkbox'] ?? [];
    $formHandler = new DeleteProductFormHandler($skuArray);

    $error = $formHandler->deleteData();
  }
  
?>



<?php $page_title = 'Juniortest - Products List'; ?>
<?php require_once "header.php"; ?>
 
<main id="content">
  <div id="main-menu">
    <form class="actions" action="<?php echo URL_BASE . "/"; ?>" method="post">
      <div class="titleArea">
        <span class="viewTitle">Product List</span>
        <div class="actionButton">
          <a class="action" href="<?php echo URL_BASE . "/addproduct"; ?>"><button type="button" class="btn btn-primary">ADD</button></a>
        </div>
        <div class="actionButton">
          <a class="action" href="/"><button id="delete-product-btn" type="submit" class="btn btn-danger">MASS DELETE</button></a>
        </div>
      </div>
    <hr>
    <div class="row">
  	<!-- <table class="list">
  	  <tr>
        <th>SKU</th>
        <th>Name</th>
        <th>Price</th>
  	    <th>Description</th>
  	    <th>Size</th>
  	    <th>Delete</th>
        
  	  </tr> -->
      <?php
          $productsView = new ProductsList([new DvdView(), new BookView(), new FurnitureView()]);
          $productsView->showAllProducts();
      ?>
  	<!-- </table> -->
    </div>
    
</form>
</main>  


<?php require_once "footer.php"; ?>

    