<?php 

  use Source\Handlers\AddProductFormHandler;

  $error = false;
  $sku = $_POST['sku'] ?? '';
  $name = $_POST['name'] ?? '';
  $price = $_POST['price'] ?? 0;
  // $stock = $_POST['stock'] ?? 1;
  // $description = $_POST['description'] ?? '';
  $size = $_POST['size'] ?? 0;
  $weight = $_POST['weight'] ?? 0;
  $height = $_POST['height'] ?? 0;
  $width = $_POST['width'] ?? 0;
  $length = $_POST['length'] ?? 0;
  

  if (is_post_request()){
    
    $data = $_POST;

    $formHandler = new AddProductFormHandler($data);

    $error = $formHandler->submitData();

    if (!$error){
      redirect_to(URL_BASE);
    }
  }
?>

<?php $page_title = 'Add Product'; ?>
<?php require_once "header.php"; ?>

<main id="content">
  <div id="main-menu">
    <form action="<?php echo URL_BASE . "/addproduct"; ?>" method="post" id="product_form">
    <div class="titleArea">
      <span class="viewTitle">Product Add</span>
      <div class="actionButton">
        <a class="action"><button type="submit" class="btn btn-primary">Save</button></a>
      </div>
      <div class="actionButton">
        <a class="action" href="<?php echo URL_BASE . "/"; ?>"><button type="button" class="btn btn-danger">Cancel</button></a>
      </div>
    </div>
    <br>
    <?php 
      if ($error !== false){
        echo "<div class='alert alert-danger alert-dismissible fade show' role='alert'>
                {$error}
                <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                  <span aria-hidden='true'>&times;</span>
                </button>
              </div>";
      }
     ?>
      <hr>
      <div class="form-group">
        <label for="sku">SKU</label>
        <input class="form-control" id="sku" name="sku" type="text" maxlength="45" value="<?php echo h($sku); ?>" required>
      </div>
      <div class="form-group">
        <label for="name">Name</label>
        <input class="form-control" id="name" name="name" type="text" maxlength="45" value="<?php echo h($name); ?>" required>
      </div>
      <div class="form-group">
        <label for="price">Price</label>
        <input class="form-control" id="price" name="price" type="number" value="<?php echo h($price); ?>" required>
      </div>

      <div class="form-group">
        <label for="productType">Please, select the product type</label>
        <select name="productType" id="productType" required>
          <option value="" selected="selected">Type Switcher</option>
          <option value="Dvd">DVD</option>
          <option value="Furniture">Furniture</option>
          <option value="Book">Book</option>
        </select>
      </div>
      <br/>

      <div class="form-group special" id="dvdInputs" style="display:none">
        <p><b>Please, provide size in Mega Bytes</b></p>
        <label for="size">Size (MB)</label>
        <input class="form-control" id="size" name="size" type="number" step="0.1" value="<?php echo h($size); ?>" >
      </div>

      <div class="form-group special" id="furnitureInputs" style="display:none">
        <p>Please, provide dimensions in HxWxL centimeters</p>
        <label for="height">Height (cm)</label>
        <input class="form-control" id="height" name="height" type="number" step="0.1" value="<?php echo h($height); ?>" >
        <label for="width">Width (cm)</label>
        <input class="form-control" id="width" name="width" type="number" step="0.1" value="<?php echo h($width); ?>" >
        <label for="length">Length (cm)</label>
        <input class="form-control" id="length" name="length" type="number" step="0.1" value="<?php echo h($length); ?>" >
      </div>

      <div class="form-group special" id="bookInputs" style="display:none">
        <p>Please, provide weight in kilograms</p>
        <label for="weight">Weight (Kg)</label>
        <input class="form-control" id="weight" name="weight" type="text" step="0.1" value="<?php echo h($weight); ?>" >
      </div>   
      
      <!-- <div class="form-group" id="stockInput">
        <p>Please, provide a quantity</p>
        <label for="stock">Quantity</label>
        <input class="form-control" type="number" id="stock" name="stock" min="1" max="999" value="<?php echo h($stock); ?>" required>
      </div>   

      <div class="form-group" id="descriptionInput" >
        <label for="description">Product description</label>
        <textarea class="form-control" id="description" name="description" type="text"><?php echo h($description); ?></textarea>
      </div>    -->
    </form>
  </div>
</main> 

<?php require_once "footer.php"; ?>