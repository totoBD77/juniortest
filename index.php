<?php

  require __DIR__ . "/vendor/autoload.php";
  require __DIR__ . "/vendor/functions.php";

  use CoffeeCode\Router\Router;

  $router = new Router(URL_BASE);

  /**
   * Controllers
   * 
   */
  $router->namespace("Source\App");

  /**
   * Web
   * 
   */

  $router->group(null);
  $router->get("/", "Web:home");
  $router->post("/", "Web:home");
  $router->get("/addproduct", "Web:addproduct");  
  $router->post("/addproduct", "Web:addproduct");  

  /**
   * Errors
   * 
   */

  $router->group("ops");
  $router->get("/{errorcode}", "Web:error");



  $router->dispatch();

  if ($router->error()){
    $router->redirect("/ops/{$router->error()}");
  }

?>