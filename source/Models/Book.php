<?php

  namespace Source\Models;

  class Book extends Product {

    private float $weight;

    public function __construct() {
      
    }

    protected function getAllProducts(){
      $sql = "SELECT products.sku, products.name, products.price, books.weight ";
      $sql .= "FROM products INNER JOIN books ";
      $sql .= "ON products.sku=books.sku";
      $stmt = $this->connect()->prepare($sql);
      $stmt->execute();
      $results = $stmt->fetchAll();

      return $results;
    }

    protected function getProduct(string $sku){
      $sql = "SELECT products.sku, products.name, products.price, books.weight ";
      $sql .= "FROM products INNER JOIN books ";
      $sql .= "ON products.sku=books.sku ";
      $sql .="WHERE books.sku = ?";
      $stmt = $this->connect()->prepare($sql);
      $stmt->execute([$sku]);
      $results = $stmt->fetchAll();
      
      if (count($results) > 0){
        return $results[0];
      } 
      return ['sku' => '', 'name' => '', 'price' => 0, 'weight' => 0];
    }

    protected function setProduct(string $sku, string $name, float $price, float $weight = 0){
      parent::setProduct($sku,$name,$price);
      $sql = "INSERT INTO books (sku, weight) ";
      $sql .= "VALUES (?, ?)";
      $stmt = $this->connect()->prepare($sql);
      return $stmt->execute([$sku, $weight]);
    }
    
    public function setWeight(float $weight) {
      $this->weight = $weight;
    }

    public function getWeight(){
      return $this->weight;
    }
  }

?>