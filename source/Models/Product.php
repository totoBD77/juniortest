<?php

  namespace Source\Models;
  use Source\Database\DBConnection;

  abstract class Product extends DBConnection {

    private string $sku;
    private string $name;
    private float $price;
    // private int $stock;
    // private string $description;

    public function __construct() {
      
    }

    protected function existsProduct(string $sku){
      $sql = "SELECT sku FROM products ";
      $sql .= "WHERE sku = ?";
      $stmt = $this->connect()->prepare($sql);

      if (!$stmt->execute([$sku])){
        $stmt = null;
        exit();
      }

      if($stmt->rowCount() > 0){
        return true;
      } 
      return false;
    }


    protected function setProduct(string $sku, string $name, float $price){
      $sql = "INSERT INTO products (sku, name, price) ";
      $sql .= "VALUES (?, ?, ?)";
      $stmt = $this->connect()->prepare($sql);
      return $stmt->execute([$sku, $name, $price]);
    }

    protected function getProduct(string $sku){
      $sql = "SELECT * FROM products ";
      $sql .="WHERE sku = ?";
      $stmt = $this->connect()->prepare($sql);
      $stmt->execute([$sku]);
      $results = $stmt->fetchAll();

      if (count($results) > 0){
        return $results[0];
      } 
      return false;
    }

    protected function deleteManyBySku($skuArray){
      $in  = str_repeat('?,', count($skuArray) - 1) . '?';
      $sql = "DELETE FROM products ";
      $sql .= "WHERE sku IN ($in)";
      $stmt = $this->connect()->prepare($sql);
      $stmt->execute($skuArray);

      return false;
    }

    // protected function modifyStockByQuantity(int $quantity, string $sku){
    //   $sql = "UPDATE products ";
    //   $sql .= "SET stock = stock + ? ";
    //   $sql .= "WHERE sku = ?";
    //   $stmt = $this->connect()->prepare($sql);
    //   $result = $stmt->execute([$quantity,$sku]);
    // }

    public function setSku(string $sku) {
      $this->sku = $sku;
    }

    public function getSku(){
      return $this->sku;
    }

    public function setName(string $name) {
      $this->name = $name;
    }

    public function getName(){
      return $this->name;
    }

    public function setPrice(float $price) {
      $this->price = $price;
    }

    public function getPrice(){
      return $this->price;
    }

    // public function setStock(int $stock) {
    //   $this->stock = $stock;
    // }

    // public function getStock(){
    //   return $this->stock;
    // }

    // public function setDescription(string $description) {
    //   $this->description = $description;
    // }

    // public function getDescription(){
    //   return $this->description;
    // }
  }

?>