<?php

  namespace Source\Models;

  class Dvd extends Product {

    private float $size;

    public function __construct() {
      
    }

    protected function getAllProducts(){
      $sql = "SELECT products.sku, products.name, products.price, dvds.size ";
      $sql .= "FROM products INNER JOIN dvds ";
      $sql .= "ON products.sku=dvds.sku";
      $stmt = $this->connect()->prepare($sql);
      $stmt->execute();
      $results = $stmt->fetchAll();

      return $results;
    }

    protected function getProduct(string $sku){
      $sql = "SELECT products.sku, products.name, products.price, dvds.size ";
      $sql .= "FROM products INNER JOIN dvds ";
      $sql .= "ON products.sku=dvds.sku ";
      $sql .="WHERE dvds.sku = ?";
      $stmt = $this->connect()->prepare($sql);
      $stmt->execute([$sku]);
      $results = $stmt->fetchAll();
      
      if (count($results) > 0){
        return $results[0];
      } 
      return ['sku' => '', 'name' => '', 'price' => 0, 'size' => 0];;
    }

    protected function setProduct(string $sku, string $name, float $price, float $size = 0){
      parent::setProduct($sku,$name,$price);
      $sql = "INSERT INTO dvds (sku, size) ";
      $sql .= "VALUES (?, ?)";
      $stmt = $this->connect()->prepare($sql);
      return $stmt->execute([$sku, $size]);
    }

    // protected function deleteManyBySku($skuArray){
    //   $in  = str_repeat('?,', count($skuArray) - 1) . '?';
    //   $sql = "DELETE FROM products ";
    //   $sql .= "WHERE sku IN ($in)";
    //   $stmt = $db->prepare($sql);
    
    //   return $stmt->execute($skuArray);
    // }
    
    public function setSize(float $size) {
      $this->size = $size;
    }

    public function getSize(){
      return $this->size;
    }
  }

?>