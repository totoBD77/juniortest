<?php

  namespace Source\Models;

  class Furniture extends Product {

    private float $height;
    private float $width;
    private float $length;

    public function __construct() {
      
    }

    protected function getAllProducts(){
      $sql = "SELECT products.sku, products.name, products.price, furniture.height, furniture.width, furniture.length ";
      $sql .= "FROM products INNER JOIN furniture ";
      $sql .= "ON products.sku=furniture.sku";
      $stmt = $this->connect()->prepare($sql);
      $stmt->execute();
      $results = $stmt->fetchAll();

      return $results;
    }

    protected function getProduct(string $sku){
      $sql = "SELECT products.sku, products.name, products.price, furniture.height, furniture.width, furniture.length FROM products INNER JOIN furniture ON products.sku=furniture.sku ";
      $sql .="WHERE furniture.sku = ?";
      $stmt = $this->connect()->prepare($sql);
      $stmt->execute([$sku]);
      $results = $stmt->fetchAll();
      
      if (count($results) > 0){
        return $results[0];
      } 
      return ['sku' => '', 'name' => '', 'price' => 0, 'height' => 0, 'width' => 0, 'length' => 0];
    }

    protected function setProduct(string $sku, string $name, float $price, float $height = 0, float $width = 0, float $length = 0){
      parent::setProduct($sku,$name,$price);
      $sql = "INSERT INTO furniture (sku, height, width, length) ";
      $sql .= "VALUES (?, ?, ?, ?)";
      $stmt = $this->connect()->prepare($sql);
      return $stmt->execute([$sku, $height, $width, $length]);
    }
    
    public function setHeight(float $height) {
      $this->height = $height;
    }

    public function getHeight(){
      return $this->height;
    }

    public function setWidth(float $width) {
      $this->width = $width;
    }

    public function getWidth(){
      return $this->width;
    }

    public function setLength(float $length) {
      $this->length = $length;
    }

    public function getLength(){
      return $this->length;
    }
  }

?>