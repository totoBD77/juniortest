<?php
  
  namespace Source\Database;
  use \PDO;
  
  class DBConnection {

    private $host = DB_SERVER;
    private $user = DB_USER;
    private $pwd = DB_PASSWORD;
    private $dbName = DB_NAME;

    protected function connect(){
      $dsn = "mysql:host=". $this->host . ";dbname=". $this->dbName .";charset=utf8mb4";
      $options = [
        PDO::ATTR_EMULATE_PREPARES   => false, 
        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC, 
      ];
      try {
        $pdo = new PDO($dsn, $this->user, $this->pwd, $options);
      } catch (PDOException $e) {
        print "Error!: " . $e->getMessage() . "<br />";
        die();
      }
      return $pdo;
    }

  }

?>