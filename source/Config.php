<?php
  declare(strict_types = 1);
  // define("URL_BASE", "https://juniortest-juan-porto.000webhostapp.com");
  define("URL_BASE", "http://www.localhost/juniorproducts");
  define("DB_SERVER", "localhost");
  // define("DB_USER", "id18619194_testuser");
  define("DB_USER", "testuser"); //or use root with own password
  // define("DB_PASSWORD", "aV@q)nLFm8uHZ3Kx");
  define("DB_PASSWORD", "password");
  // define("DB_NAME","id18619194_junior_products");
  define("DB_NAME", "junior_products");
  
  ob_start(); //output buffering on
?>