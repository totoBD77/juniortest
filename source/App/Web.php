<?php

  namespace Source\App;

  class Web {
    
    public function home($data){
      require_once  $_SERVER['DOCUMENT_ROOT'] . "/juniorproducts/templates/index.php";
      // echo "<h1>Home</h1>";
      // echo "<a href=" . URL_BASE . "/addproduct>Go to add</a>";
    }

    public function addproduct($data){
      require_once  $_SERVER['DOCUMENT_ROOT'] . "/juniorproducts/templates/addproduct.php";
      // echo "<h1>Add product</h1>";
      // echo "<a href=" . URL_BASE . "/>Go to add</a>";
    }

    public function error($data){
      echo "<h1>Error: {$data["errorcode"]}</h1>";
    }
  }