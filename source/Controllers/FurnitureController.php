<?php

  namespace Source\Controllers;
  use Source\Models\Furniture;
  
  class FurnitureController extends Furniture {

    public function __construct(array $data){
      $this->setSku($data["sku"]);
      $this->setName($data["name"]);
      $this->setPrice($data["price"]);
      // $this->setStock($stock);
      // $this->setDescription($description);
      $this->setHeight($data["height"]);
      $this->setWidth($data["width"]);
      $this->setLength($data["length"]);
    }

    public function createProduct(){
      if (empty($this->getSku())){
        return "Please, provide a value for SKU.";
      }

      if ($this->invalidSku()){
        return "The provided SKU was invalid. Make sure to only use letters and numbers";
      }

      if (empty($this->getName())){
        return "Product has to have a name.";
      }

      if (strlen($this->getSku()) > 40 || strlen($this->getName()) > 40){
        return "Please insert SKU or name with no more than 40 characters.";
      }

      if (empty($this->getPrice())){
        return "Product has to have a price.";
      }

      // if (empty($this->getStock())){
      //   return "The stock field cannot be empty.";
      // }

      if (empty($this->getHeight()) || empty($this->getWidth()) || empty($this->getLength())){
        return "The dimensions fields cannot be empty.";
      }

      if ($this->getPrice() <= 0 || $this->getHeight() <= 0 || $this->getWidth() <= 0 || $this->getLength() <= 0){
        return "Price and dimensions have to be greater than 0.";
      }
      
      if ($this->existsProduct(strtoupper($this->getSku()))){
        return "A product with that SKU already exists.";
      } else {
        $this->setProduct(strtoupper($this->getSku()), $this->getName(), $this->getPrice(), $this->getHeight(), $this->getWidth(), $this->getLength());
      }

      return false;
    }
    

    private function invalidSku(){
      if (!preg_match("/^[a-zA-Z0-9]*$/",$this->getSku())){
        return true;
      }
      return false;
    }


  }

?>