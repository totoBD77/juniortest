<?php

  namespace Source\Handlers;
  use Source\Controllers\{DvdController, FurnitureController, BookController};

  class AddProductFormHandler {

    private $dataController;
    private $error = false;

    public function __construct($formData = []){
      if (isset($formData["productType"])){
        $productType = ucfirst(strtolower($formData["productType"]));
        $validNumberFields = $this->validateNumberFields($formData);
        
        if ($validNumberFields){
          $data = array("sku" => $formData["sku"], "name" => $formData["name"], "price" => $formData["price"], 
                  "size" => $formData["size"], 
                  "height" => $formData["height"], "width" => $formData["width"], "length" => $formData["length"],
                  "weight" => $formData["weight"]
                  );
          if ($productType !== "Dvd" && $productType !== "Furniture" && $productType !== "Book" ){
            $this->error = "Invalid product type was submitted or none was submitted at all.";
          } else {
            $className = "Source\Controllers\\" . $productType . "Controller";
            $ref = new \ReflectionClass($className);
            $this->dataController = $ref->newInstanceArgs(array($data));
          }
        }
      }
    }

    public function submitData(){
      if ($this->error === false){
        $this->error = $this->dataController->createProduct();
      }

      return $this->error;
    }

    public function getError(){
      return $this->error;
    }

    private function validateNumberFields($formData){
      if (!is_numeric($formData["price"])){
        $this->error = "Please provide a valid number for price.";
        return false;
      }

      if (!is_numeric($formData["size"])){
        $this->error = "Please provide a valid number for size.";
        return false;
      }

      if (!is_numeric($formData["height"])){
        $this->error = "Please provide a valid number for height.";
        return false;
      }

      if (!is_numeric($formData["width"])){
        $this->error = "Please provide a valid number for width.";
        return false;
      }

      if (!is_numeric($formData["length"])){
        $this->error = "Please provide a valid number for length.";
        return false;
      }

      if (!is_numeric($formData["weight"])){
        $this->error = "Please provide a valid number for weight.";
        return false;
      }

      return true;
    }
  }

?>