<?php

  namespace Source\Handlers;
  use Source\Controllers\ProductController;

  class DeleteProductFormHandler {

    private $dataController;
    private $skuArray = [];
    private $error = false;

    public function __construct($skuArray){
      $this->skuArray = $skuArray;
      $this->dataController = new ProductController();
    }

    public function deleteData(){
      $this->error = $this->dataController->deleteProducts($this->skuArray);

      return $this->error;
    }

    public function getError(){
      return $this->error;
    }

  }