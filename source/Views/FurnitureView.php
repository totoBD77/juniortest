<?php

  namespace Source\Views;
  use Source\Models\Furniture;
  
  class FurnitureView extends Furniture {

    private static function withData(string $sku, string $name, float $price, float $height = 0, float $width = 0, float $length = 0) {
      $instance = new self();
      
      $instance->setSku($sku);
      $instance->setName($name);
      $instance->setPrice($price);
      // $instance->setStock($stock);
      // $instance->setDescription($description);
      $instance->setHeight($height);
      $instance->setWidth($width);
      $instance->setLength($length);
      return $instance;
    }

    public function showProduct(string $sku){
      $furniturearr = $this->getProduct($sku);
      $furniture = FurnitureView::withData($furniturearr['sku'],$furniturearr['name'],$furniturearr['price'],$furniturearr['height'],$furniturearr['width'],$furniturearr['length']);
      $furniture->displayProduct();
    }

    public function getAllProducts(){
      $furnitureAssocArr = parent::getAllProducts();
      $furnitureObjArr = [];
      foreach ($furnitureAssocArr as $furnitureAssoc){
        array_push($furnitureObjArr, FurnitureView::withData($furnitureAssoc['sku'],$furnitureAssoc['name'],$furnitureAssoc['price'],$furnitureAssoc['height'],$furnitureAssoc['width'],$furnitureAssoc['length']));
      }

      return $furnitureObjArr;
    }

    public function displayProduct(){
      echo '<div class="col-lg-4 col-xs-12 text-center">';
      echo '<div class="box">';
      echo "<div class='box-title'>
              {$this->getName()}
            </div>";
      echo "<div class='box-text'>
              <span><strong>SKU: </strong>{$this->getSku()}</span>
            </div>";
      echo "<div class='box-text'>
              <span><strong>Price: </strong>{$this->getPrice()}</span>
            </div>";
      echo "<div class='box-text'>
              <span><strong>Dimensions: </strong>{$this->getHeight()}x{$this->getWidth()}x{$this->getLength()}</span>
            </div>";
      // echo "<div class='box-text'>
      //         <span><strong>Description: </strong>{$this->getDescription()}</span>
      //       </div>";  
      echo "<div class='box-text deleteBox'>
              <label>Delete</label>
              <input class='delete-checkbox' name='checkbox[]' type='checkbox' value='{$this->getSku()}'>
            </div>";  
      echo "</div></div>";
    }
  }

?>