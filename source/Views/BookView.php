<?php

  namespace Source\Views;
  use Source\Models\Book;

  class BookView extends Book {

    private static function withData(string $sku, string $name, float $price, float $weight = 0) {
      $instance = new self();
      
      $instance->setSku($sku);
      $instance->setName($name);
      $instance->setPrice($price);
      // $instance->setStock($stock);
      // $instance->setDescription($description);
      $instance->setWeight($weight);
      return $instance;
    }

    // public function showProduct(string $sku){
    //   $bookarr = $this->getProduct($sku);
    //   $book = BookView::withData($bookarr['sku'],$bookarr['name'],$bookarr['price'],$bookarr['weight']);
    //   $book->displayProduct();
    // }

    public function getAllProducts(){
      $booksAssocArr = parent::getAllProducts();
      $booksObjArr = [];
      foreach ($booksAssocArr as $bookAssoc){
        array_push($booksObjArr, BookView::withData($bookAssoc['sku'],$bookAssoc['name'],$bookAssoc['price'],$bookAssoc['weight']));
      }

      return $booksObjArr;
    }

    public function displayProduct(){
      echo '<div class="col-lg-4 col-xs-12 text-center">';
      echo '<div class="box">';
      echo "<div class='box-title'>
              {$this->getName()}
            </div>";
      echo "<div class='box-text'>
              <span><strong>SKU: </strong>{$this->getSku()}</span>
            </div>";
      echo "<div class='box-text'>
              <span><strong>Price: </strong>{$this->getPrice()}</span>
            </div>";
      echo "<div class='box-text'>
              <span><strong>Weight: </strong>{$this->getWeight()}</span>
            </div>";
      // echo "<div class='box-text'>
      //         <span><strong>Description: </strong>{$this->getDescription()}</span>
      //       </div>";  
      echo "<div class='box-text deleteBox'>
              <label>Delete</label>
              <input class='delete-checkbox' name='checkbox[]' type='checkbox' value='{$this->getSku()}'>
            </div>";  
      echo "</div></div>";
    }
  }

?>