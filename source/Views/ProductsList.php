<?php

  namespace Source\Views;

  class ProductsList {

    private $views = [];

    public function __construct($views = []){
      $this->views = $views;
    }

    public function showAllProducts(){
      $products = [];
      foreach ($this->views as $view){
        $products = array_merge($products, $view->getAllProducts());
      }
      
      usort($products, function($a, $b){ return strcmp($a->getSku(), $b->getSku()); });

      foreach ($products as $product){
        // for ($i = 1; $i <= $product->getStock(); $i++) {
        $product->displayProduct();
        // }
      }

    }


  }

?>