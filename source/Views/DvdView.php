<?php

  namespace Source\Views;
  use Source\Models\Dvd;
  
  class DvdView extends Dvd {

    private static function withData(string $sku, string $name, float $price, float $size = 0) {
      $instance = new self();
      
      $instance->setSku($sku);
      $instance->setName($name);
      $instance->setPrice($price);
      // $instance->setStock($stock);
      // $instance->setDescription($description);
      $instance->setSize($size);
      return $instance;
    }

    // public function showProduct(string $sku){
    //   $dvdarr = $this->getProduct($sku);
    //   $dvd = DvdView::withData($dvdarr['sku'],$dvdarr['name'],$dvdarr['price'],$dvdarr['stock'],$dvdarr['size']);
    //   $dvd->displayProduct();
    // }

    public function getAllProducts(){
      $dvdsAssocArr = parent::getAllProducts();
      $dvdsObjArr = [];
      foreach ($dvdsAssocArr as $dvdAssoc){
        array_push($dvdsObjArr, DvdView::withData($dvdAssoc['sku'],$dvdAssoc['name'],$dvdAssoc['price'],$dvdAssoc['size']));
      }

      return $dvdsObjArr;
    }
      
    public function displayProduct(){
      echo '<div class="col-lg-4 col-xs-12 text-center">';
      echo '<div class="box">';
      echo "<div class='box-title'>
              {$this->getName()}
            </div>";
      echo "<div class='box-text'>
              <span><strong>SKU: </strong>{$this->getSku()}</span>
            </div>";
      echo "<div class='box-text'>
              <span><strong>Price: </strong>{$this->getPrice()}</span>
            </div>";
      echo "<div class='box-text'>
              <span><strong>Size: </strong>{$this->getSize()}</span>
            </div>";
      // echo "<div class='box-text'>
      //         <span><strong>Description: </strong>{$this->getDescription()}</span>
      //       </div>";  
      echo "<div class='box-text deleteBox'>
              <label>Delete</label>
              <input class='delete-checkbox' name='checkbox[]' type='checkbox' value='{$this->getSku()}'>
            </div>";  
      echo "</div></div>";
    }
  }

?>